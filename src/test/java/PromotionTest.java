import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import model.Product;

/**
 * Created by c1948957 on 03/09/2016.
 */
public class PromotionTest {

    private BasketItems basketItems;
    private Catalog productCatalog;
    private List<Promotion> promotions;
    DeliveryCharge deliveryCharge;

    @Before
    public void configureTest() {
        basketItems = new BasketItems();
        productCatalog = buildCatalog();
        promotions = buildPromotions();
        deliveryCharge = buildDeliveryCharge();
    }

    @Test
    public void firstTest() {

        basketItems.add(new Product("Socks", "S01", 7.95));
        basketItems.add(new Product("Blouse", "B01", 24.95));

        Basket basket = new Basket(basketItems, productCatalog, promotions, deliveryCharge);
        double total = basket.total();

        assertEquals(37.85, total, 0);
    }

    @Test
    public void secondTest() {

        basketItems.add(new Product("Jeans", "J01", 32.95));
        basketItems.add(new Product("Jeans", "J01", 32.95));

        Basket basket = new Basket(basketItems, productCatalog, promotions, deliveryCharge);
        double total = basket.total();

        assertEquals(54.37, total, 0);
    }

    @Test
    public void thirdTest() {

        basketItems.add(new Product("Socks", "S01", 7.95));
        basketItems.add(new Product("Socks", "S01", 7.95));
        basketItems.add(new Product("Jeans", "J01", 32.95));
        basketItems.add(new Product("Jeans", "J01", 32.95));
        basketItems.add(new Product("Jeans", "J01", 32.95));

        Basket basket = new Basket(basketItems, productCatalog, promotions, deliveryCharge);
        double total = basket.total();

        assertEquals(98.27, total, 0);
    }


    @Test
    public void fourthTest() {

        basketItems.add(new Product("Blouse", "B01", 24.95));
        basketItems.add(new Product("Jeans", "J01", 32.95));

        Basket basket = new Basket(basketItems, productCatalog, promotions, deliveryCharge);
        double total = basket.total();

        assertEquals(60.85, total, 0);
    }



    private DeliveryCharge buildDeliveryCharge() {
        List<DeliveryChargeRule> rules = new ArrayList<>();

        rules.add(new DeliveryChargeRule(0, 50, 4.95));
        rules.add(new DeliveryChargeRule(50.01, 90, 2.95));
        rules.add(new DeliveryChargeRule(90.01, 0));

        return new DeliveryCharge(rules);
    }

    private Catalog buildCatalog() {
        List<Product> products = new LinkedList<>();

        products.add(new Product("Jeans", "J01", 32.95));
        products.add(new Product("Blouse", "B01", 24.95));
        products.add(new Product("Socks", "S01", 7.95));

        return new Catalog(products);
    }

    private List<Promotion> buildPromotions() {
        List<Promotion> promotions = new LinkedList<>();
        Promotion buyOneGetOneHalfPrice = new Promotion("J01", "Test Offer");
        promotions.add(buyOneGetOneHalfPrice);


        return promotions;
    }
}

