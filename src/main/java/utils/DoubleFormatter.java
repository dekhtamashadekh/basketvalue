package utils;

import java.text.DecimalFormat;

/**
 * Created by c1948957 on 03/09/2016.
 */
public class DoubleFormatter {


    private DoubleFormatter() {
    }

    public static double formatNumber(double number) {
        DecimalFormat formatter = new DecimalFormat("####.##");
        return Double.parseDouble(formatter.format(number));
    }
}
