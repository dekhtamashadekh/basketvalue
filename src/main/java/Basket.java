import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import model.Product;
import utils.DoubleFormatter;

/**
 * Created by c1948957 on 03/09/2016.
 */
public class Basket {
    private Catalog productCatalog;
    private DeliveryCharge deliveryCharge;
    private List<Promotion> promotions;

    private BasketItems basketItems;

    public Basket(BasketItems basketItems, Catalog productCatalog, List<Promotion> promotions, DeliveryCharge deliveryCharge) {
        if(basketItems == null) {
            this.basketItems = new BasketItems();
        }

        this.productCatalog = productCatalog;
        this.promotions = promotions;
        this.deliveryCharge = deliveryCharge;
        this.basketItems = basketItems;
    }

    public boolean add(String productCode) {
        Optional<Product> product = productCatalog.getProduct(productCode);

        if(product.isPresent()) {
            basketItems.add(product.get());
            return true;
        }
        return false;
    }

    public double total() {

        if(basketItems.isEmpty()) {
            return 0;
        }

        double netTotal = grossTotal() - discountValue();

        return DoubleFormatter.formatNumber(netTotal + calculateDeliveryCharges());
    }

    private double discountValue() {
        return promotions.stream().mapToDouble(o -> o.applyToList(basketItems)).sum();
    }

    private double calculateDeliveryCharges() {
        return deliveryCharge.applyToAmount(grossTotal() - discountValue());
    }

    private double grossTotal() {

        Set<Product> shoppingListProducts = basketItems.getProducts();
        List<String> productCodes = shoppingListProducts.stream().map(p -> p.getCode()).collect(Collectors.toList());

        return productCodes.stream().mapToDouble(p -> productCatalog.getPriceByCode(p) * basketItems.getOccurrences(p)).sum();
    }
}
