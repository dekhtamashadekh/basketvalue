package model;

/**
 * Created by c1948957 on 03/09/2016.
 */
public class Product {

    private final String name;
    private final String code;
    private double price;

    public Product(String name, String code, double price) {
        this.name = name;
        this.code = code;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }


    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (!Product.class.isAssignableFrom(object.getClass())) {
            return false;
        }

        final Product product = (Product) object;

        return name.equals(product.getName()) &&
               code.equals(product.getCode()) &&
               price == product.getPrice();
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }
}

