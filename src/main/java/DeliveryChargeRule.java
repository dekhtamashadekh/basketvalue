/**
 * Created by c1948957 on 03/09/2016.
 */
public class DeliveryChargeRule {

    private double lowerLimit;
    private double upperLimit;
    private double charge;

    public DeliveryChargeRule(double lowerLimit, double upperLimit, double charge) {
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.charge = charge;
    }

    public DeliveryChargeRule(double lowerLimit, double charge) {
        this.lowerLimit = lowerLimit;
        this.upperLimit = -1;
        this.charge = charge;
    }


    public double getCharge() {
        return charge;
    }


    public boolean isInRange(double amount) {

        if (upperLimit == -1) {
            return amount > lowerLimit;
        }

        return amount > lowerLimit && amount < upperLimit;
    }
}

