import java.util.List;

/**
 * Created by c1948957 on 03/09/2016.
 */
public class DeliveryCharge {

    List<DeliveryChargeRule> rules;

    public DeliveryCharge(List<DeliveryChargeRule> newRules) {
        rules = newRules;
    }


    public double applyToAmount(double total) {
        return rules.stream().mapToDouble(a -> a.isInRange(total) ? a.getCharge() : 0).sum();
    }


}

