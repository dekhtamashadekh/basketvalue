import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import model.Product;

/**
 * Created by c1948957 on 03/09/2016.
 */
public class Catalog {

    private Set<Product> productCatalog = null;


    public Catalog(List<Product> products) {
        if (productCatalog == null) {
            productCatalog = new HashSet<Product>();
        }

        productCatalog.addAll(products);
    }


    public double getPriceByCode(String productCode)  {
        Optional<Product>
                productByCode =
                productCatalog.stream().filter(p -> productCode.equals(p.getCode())).findFirst();

        if (productByCode.isPresent()) {
            return productByCode.get().getPrice();
        }

        return 0.0;
    }

    public Optional<Product> getProduct(String productCode) {
        Optional<Product> product = productCatalog.stream().filter(p -> productCode.equals(p.getCode())).findFirst();

        if (product.isPresent()) {
            return product;
        }

        return Optional.empty();
    }

}

