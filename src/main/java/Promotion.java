import java.util.Optional;

import model.Product;
import utils.DoubleFormatter;

/**
 * Created by c1948957 on 03/09/2016.
 */


public class Promotion  {

    private String offerName;
    private String productCode;

    public Promotion(String productCode, String offerName) {
        this.productCode = productCode;
        this.offerName = offerName;
    }


    public double applyToList(BasketItems basketItems) {
        double discount = 0.0;

        Optional<String> productCodeOnOffer = basketItems.getProducts()
                .stream()
                .filter(p -> productCode.equals(p.getCode()))
                .map(p -> p.getCode()).findFirst();

        if (!productCodeOnOffer.isPresent()) {
            return 0.0;
        }

        Optional<Product> productOnOfferOptional = basketItems.getProduct(productCodeOnOffer.get());

        Product productOnOffer = productOnOfferOptional.get();
        double itemsToDiscount = basketItems.getOccurrences(productOnOffer.getCode()) / 2;
        discount += itemsToDiscount * productOnOffer.getPrice() / 2;

        return DoubleFormatter.formatNumber(discount);
    }
}

