import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import model.Product;

/**
 * Created by c1948957 on 03/09/2016.
 */
public class BasketItems {

    Map<Product, Integer> map;

    public BasketItems() {
        map = new HashMap<>();
    }

    public void add(Product product) {
        if (map == null) {
            map = new HashMap<>();
        }

        List<String> productCodes = map.keySet().stream().map(p -> p.getCode()).collect(Collectors.toList());

        if (map.isEmpty() || !productCodes.contains(product.getCode())) {
            map.put(product, 1);
        } else {
            increaseOccurrence(product);
        }
    }

    private void increaseOccurrence(Product product) {
        int productOccurrence = getOccurrences(product.getCode());
        productOccurrence++;
        map.replace(product, productOccurrence);
    }


    public Set<Product> getProducts() {
        return map.keySet();
    }

    public int getOccurrences(String productCode) {
        Optional<Product> product = getProduct(productCode);
        if (product.isPresent()) {
            return map.get(product.get());
        }
        return 0;
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }


    public Optional<Product> getProduct(String productCode) {
        Set<Product> products = map.keySet();

        List<Product>
                productList =
                products.stream().filter(p -> productCode.equals(p.getCode())).collect(Collectors.toList());

        if (productList.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(productList.get(0));
        }
    }


}
